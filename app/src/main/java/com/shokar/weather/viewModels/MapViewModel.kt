package com.shokar.weather.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.shokar.weather.models.CityInfoModel

class MapViewModel : ViewModel() {
    val cityInfo: MutableLiveData<CityInfoModel> by lazy {
        MutableLiveData<CityInfoModel>()
    }

    //fun cityInfo() = cityInfo as LiveData<CityInfoModel>
}