package com.shokar.weather.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.shokar.weather.domain.WeatherInteractorInterface
import com.shokar.weather.models.WeatherModel

class WeatherViewModel(private val repository: WeatherInteractorInterface) : ViewModel() {

    private var weather = MutableLiveData<WeatherModel>()

    fun getWeather(lat: String, lon: String, appid: String): LiveData<WeatherModel>{
        weather = repository.weather
        return repository.getWeather(lat, lon, appid)
    }

    fun weather() = weather as LiveData<WeatherModel>
}