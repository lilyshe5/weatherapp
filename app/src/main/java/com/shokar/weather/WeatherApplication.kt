package com.shokar.weather

import android.app.Application
import com.shokar.weather.di.remoteDatasourceModule
import com.shokar.weather.modules.Modules.mapViewModel
import com.shokar.weather.modules.Modules.weatherViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class WeatherApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@WeatherApplication)
            modules(remoteDatasourceModule)
            modules(mapViewModel)
            modules(weatherViewModel)
        }

    }
}