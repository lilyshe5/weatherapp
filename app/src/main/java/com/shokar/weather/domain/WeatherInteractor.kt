package com.shokar.weather.domain

import android.service.autofill.Validators.not
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.shokar.weather.data.WeatherDataInterface
import com.shokar.weather.models.OpenWeatherModel
import com.shokar.weather.models.WeatherModel
import kotlinx.coroutines.*
import retrofit2.HttpException
import retrofit2.Response
import java.text.DecimalFormat
import kotlin.math.roundToInt

class WeatherInteractor(
    private val weatherDataInterface: WeatherDataInterface
) : WeatherInteractorInterface {

    override val weather: MutableLiveData<WeatherModel> by lazy {
        MutableLiveData<WeatherModel>()
    }

    private var viewModelJob = Job()
    private val viewModelScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    override fun getWeather(lat: String, lon: String, appid: String): LiveData<WeatherModel> {

        viewModelScope.launch {

            val response = weatherDataInterface.getWeatherData(lat, lon, appid)
            withContext(Dispatchers.Main) {
                try {
                    if (response.isSuccessful) {
                        weather.postValue(toWeatherModel(response))
                    } else {

                    }
                } catch (e: HttpException) {

                } catch (e: Throwable) {

                }
            }
        }

        return weather
    }

    private fun toWeatherModel(response: Response<OpenWeatherModel>): WeatherModel {
        val weatherModel = WeatherModel()

        val body = response.body()

        //converting hPa to mm Hg
        weatherModel.pressure = body?.main?.pressure?.div(HG)

        //converting from Kelvin to Celsius
        weatherModel.degree = body?.main?.temp?.minus(KELVIN)

        weatherModel.weatherDescription = body?.weather?.get(0)?.description
        weatherModel.icon = body?.weather?.get(0)?.icon
        weatherModel.humidity = body?.main?.humidity
        weatherModel.cityName = body?.name
        weatherModel.wind = body?.wind?.speed

        return weatherModel
    }

    companion object {
        const val KELVIN = 273.15
        const val HG = 1.33322387415
    }
}

