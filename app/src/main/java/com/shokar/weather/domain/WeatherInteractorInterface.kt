package com.shokar.weather.domain

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.shokar.weather.models.WeatherModel

interface WeatherInteractorInterface {
    val weather: MutableLiveData<WeatherModel>

    fun getWeather(lat: String, lon: String, appid: String): LiveData<WeatherModel>
}