package com.shokar.weather.models

data class CityInfoModel(
    var cityName: String? = null,
    var lon: Double? = null,
    var lat: Double? = null
)