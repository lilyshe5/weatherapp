package com.shokar.weather.models

data class WeatherModel(
    var cityName: String? = null,
    var weatherDescription: String? = null,
    var degree: Double? = null,
    var wind: Double? = null,
    var pressure: Double? = null,
    var humidity: Int? = null,
    var icon: String? = null
)