package com.shokar.weather.models

data class OpenWeatherModel(
    val coord: Coord? = null,
    val weather: MutableList<Weather>? = null,
    val base: String? = null,
    val main: Main? = null,
    val wind: Wind? = null,
    val clouds: Clouds? = null,
    val dt: Int? = null,
    val sys: Sys? = null,
    val id: Int? = null,
    val name: String? = null,
    val cod: Int? = null
)

data class Coord(
    val lon: Double? = null,
    val lat: Double? = null
)

data class Weather(
    val id: Int? = null,
    val main: String? = null,
    val description: String? = null,
    val icon: String? = null
)

data class Main(
    val temp: Double? = null,
    val pressure: Int? = null,
    val humidity: Int? = null,
    val temp_min: Double? = null,
    val temp_max: Double? = null,
    val sea_level: Double? = null,
    val grnd_level: Double? = null
)

data class Wind(
    val speed: Double? = null,
    val deg: Int? = null
)

data class Clouds(val all: Int? = null)

data class Sys(
    val message: Double? = null,
    val country: String? = null,
    val sunrise: Int? = null,
    val sunset: Int? = null
)