package com.shokar.weather.di

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.shokar.weather.data.repositories.WeatherApi
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val remoteDatasourceModule = module {
    single { createWebService() }
}

fun createWebService(): WeatherApi {
    val retrofit = Retrofit.Builder()
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl("http://api.openweathermap.org/data/2.5/")
        .build()
    return retrofit.create(WeatherApi::class.java)
}