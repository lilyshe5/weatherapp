package com.shokar.weather.modules

import com.shokar.weather.R
import com.shokar.weather.data.WeatherData
import com.shokar.weather.data.WeatherDataInterface
import com.shokar.weather.di.createWebService
import com.shokar.weather.domain.WeatherInteractor
import com.shokar.weather.domain.WeatherInteractorInterface
import com.shokar.weather.viewModels.MapViewModel
import com.shokar.weather.viewModels.WeatherViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object Modules {
    val weatherViewModel = module {
        single<WeatherDataInterface> { WeatherData(get()) }
        single<WeatherInteractorInterface> { WeatherInteractor(get()) }
        viewModel { WeatherViewModel(get()) }
    }

    val mapViewModel = module {
        viewModel { MapViewModel() }
    }
}