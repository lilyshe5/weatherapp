package com.shokar.weather.views

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.*
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.shokar.weather.R
import com.shokar.weather.databinding.ActivityMapsBinding
import com.shokar.weather.models.CityInfoModel
import com.shokar.weather.viewModels.MapViewModel
import kotlinx.android.synthetic.main.activity_maps.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private val model: MapViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityMapsBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_maps)

        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                0
            )
        }

        binding.run {
            viewModel = model
            lifecycleOwner = this@MapsActivity
        }

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        buttonShowWeather.setOnClickListener {
            val intent = Intent(this, WeatherActivity::class.java)

            intent.putExtra(LAT, model.cityInfo.value?.lat.toString())
            intent.putExtra(LON, model.cityInfo.value?.lon.toString())
            intent.putExtra(CITYNAME, model.cityInfo.value?.cityName)

            startActivity(intent)
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            0 -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {

                } else {

                }
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        try {
            val location =
                locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)

            val latLng = LatLng(location.latitude, location.longitude)
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12.0f))
            googleMap.addMarker(MarkerOptions().position(latLng))
        } catch (e: SecurityException) {

        }

        googleMap.setOnMapClickListener { latLng ->
            bottomPanel.visibility = View.VISIBLE

            val panelHeight = bottomPanel.minHeight
            bottomPanel.animate().translationY(-panelHeight.toFloat()).duration = 500L

            val geo = Geocoder(this@MapsActivity, Locale.getDefault())

            val address: List<Address?> =
                geo.getFromLocation(latLng.latitude, latLng.longitude, 1)

            if (address.isNotEmpty()) {
                googleMap.clear()
                googleMap.addMarker(MarkerOptions().position(latLng))

                model.cityInfo.postValue(addressToWeatherModel(address))
            }
        }
    }

    private fun addressToWeatherModel(address: List<Address?>): CityInfoModel {
        val cityInfoModel = CityInfoModel()

        cityInfoModel.cityName = address[0]?.locality
        cityInfoModel.lat = address[0]?.latitude
        cityInfoModel.lon = address[0]?.longitude

        return cityInfoModel
    }


    companion object {
        const val LAT = "LAT"
        const val LON = "LON"
        const val CITYNAME = "CITYNAME"
    }
}
