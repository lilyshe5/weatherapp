package com.shokar.weather.views

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.shokar.weather.R
import com.shokar.weather.databinding.ActivityWeatherBinding
import com.shokar.weather.models.WeatherModel
import com.shokar.weather.viewModels.WeatherViewModel
import kotlinx.android.synthetic.main.activity_weather.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class WeatherActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityWeatherBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_weather)

        val model: WeatherViewModel by viewModel()

        val cityName = intent.getStringExtra(MapsActivity.CITYNAME)
        val lat = intent.getStringExtra(MapsActivity.LAT)
        val lon = intent.getStringExtra(MapsActivity.LON)
        val appid = resources.getString(R.string.open_weather_key)

        title = cityName

        model.getWeather(lat, lon, appid)

        binding.run {
            viewModel = model
            lifecycleOwner = this@WeatherActivity
        }

        val sth = Observer<WeatherModel> { weather ->
            Glide.with(this)
                .load("$URI${model.weather().value?.icon}$FORMAT")
                .into(weatherIcon)

            progressBar.visibility = View.GONE
        }

        model.weather().observe(this, sth)

    }

    override fun onResume() {
        super.onResume()

        progressBar.visibility = View.VISIBLE
    }

    companion object {
        const val URI = "http://openweathermap.org/img/wn/"
        const val FORMAT = ".png"
    }
}