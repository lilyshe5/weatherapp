package com.shokar.weather.data.repositories

import com.shokar.weather.models.OpenWeatherModel
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    @GET("weather?")
    fun getCurrentWeather(
        @Query("lat") lat: String,
        @Query("lon") lon: String,
        @Query("appid") appid: String
    ):
            Deferred<Response<OpenWeatherModel>>
}

