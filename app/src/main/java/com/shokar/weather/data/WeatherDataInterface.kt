package com.shokar.weather.data

import com.shokar.weather.models.OpenWeatherModel
import kotlinx.coroutines.Deferred
import retrofit2.Response

interface WeatherDataInterface {

    suspend fun getWeatherData(lat: String, lon: String, appid: String): Response<OpenWeatherModel>
}