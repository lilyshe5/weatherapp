package com.shokar.weather.data

import com.shokar.weather.data.repositories.WeatherApi
import com.shokar.weather.models.OpenWeatherModel
import retrofit2.Response

class WeatherData(private val weatherService: WeatherApi) : WeatherDataInterface {

    override suspend fun getWeatherData(lat: String, lon: String, appid: String): Response<OpenWeatherModel> {
        return weatherService.getCurrentWeather(lat, lon, appid).await()
    }
}